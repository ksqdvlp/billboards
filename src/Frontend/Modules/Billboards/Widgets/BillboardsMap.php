<?php

namespace Frontend\Modules\Billboards\Widgets;

use Frontend\Core\Engine\Base\Widget as FrontendBaseWidget;
use Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegion;
use Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegionRepository;

/**
 * Карта расположения рекламных щитов
 */
class BillboardsMap extends FrontendBaseWidget
{
    /** @var BillboardRegionRepository Репозиторий для справочника регионов. */
    private $regionRepository;

    public function execute(): void
    {
        $this->regionRepository = $this->get('doctrine')->getRepository(BillboardRegion::class);

        parent::execute();

        $this->loadTemplate();
        $this->parse();
    }

    private function parse(): void
    {
        $this->template->assign('regions', $this->regionRepository->getAllRegions());
    }
}

jsFrontend.billboards = {
  /** Yandex Map */
  yandexMap : ymaps.Map,

  /** Кластеризатор Yandex Map */
  mapClusterer: [],

  /** Текущий объекты карты */
  currentObjects: [],

  /** Окно с информацией о выбранном объекте */
  objectInfo: ymaps.Balloon,

  /** Корзина */
  $cart : [],

  /** Содержимое корзины */
  cartContent: [],

  /** Инициализация JavaScript */
  init : function() {
    jsFrontend.billboards.initYandexMap();
  },

  /** Инициализация карты Yandex */
  initYandexMap: function () {
    ymaps.ready(function () {
      jsFrontend.billboards.yandexMap = new ymaps.Map('billboardsMap', {
        center: [55.76, 37.64],
        zoom: 10,
        controls: []
      });

      // Инициализируем всё остальное после карты, иначе... всё плохо
      jsFrontend.billboards.initRegionSelection();
      jsFrontend.billboards.initFormatSelector();
      jsFrontend.billboards.initBillboardSelector();
      jsFrontend.billboards.initCart();
    });
  },

  /** Инициализация выбора региона */
  initRegionSelection: function () {
    let $regionSelector = $('#regionSelector');

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
      $regionSelector.selectpicker('mobile');
    }

    /** Обработчик выбора региона */
    $regionSelector.on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
      let regionId = e.currentTarget[clickedIndex].value;

      jsFrontend.billboards.updateMap(regionId);
      jsFrontend.billboards.clearCart();
      jsFrontend.billboards.closeObjectInfo();
    });

    // Загружаем данные для стартового региона
    let $startRegionId = $('#regionSelector option').filter(function () { return $(this).html() == "Москва"; }).val();
    jsFrontend.billboards.updateMap($startRegionId);
  },

  /** Инициализация селектора формата */
  initFormatSelector: function() {
    let $formatSelector = $('#priceListCategorySelector');
    $formatSelector.on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
      let $selection = $formatSelector.find("option:selected");
      let selectedItems = [];
      for (let selectionIndex = 0; selectionIndex < $selection.length; selectionIndex++) {
        selectedItems.push($selection[selectionIndex].text);
      };

      let visibleObjects = [];
      jsFrontend.billboards.currentObjects.forEach(function (obj) {
        let isVisible = $.inArray(obj.info.format, selectedItems) != -1 || $selection.length == 0;

        obj.options.set('visible', isVisible);

        if (isVisible)
          visibleObjects.push(obj);
      });
      jsFrontend.billboards.mapClusterer.removeAll();
      jsFrontend.billboards.mapClusterer.add(visibleObjects);
      jsFrontend.billboards.updateBillboardSelector(visibleObjects);
    });
  },

  /** Загрузка карты укананного региона */
  updateMap: function (regionId) {
    $.ajax({
      data: {
        fork: {
          module: 'Billboards',
          action: 'GetRegion',
          language: jsFrontend.current.language
        },
        region_id: regionId
      },
      success: function (data) {
        jsFrontend.billboards.centerMap(data.data.region_name);
        jsFrontend.billboards.addObjects(data.data.billboards);
        jsFrontend.billboards.updateFormats(data.data.formats);
      },
      timeout: 60000
    });
  },

  /** Заполняем фильтр форматов */
  updateFormats: function(formats)
  {
    let $category = $('#priceListCategorySelector');
    $category.find('option').remove();

    formats.forEach(function (item) {
      $category.append('<option value="' + item.format + '">' + item.format + '</option>');
    });

    $category.selectpicker('refresh');
  },

  /** Выполняет добавление объектов на карту. */
  addObjects: function(objects) {
    // Делаем копию списка текущих объектов и очищаем его
    let oldObjects = jsFrontend.billboards.currentObjects;
    let oldClusterer = jsFrontend.billboards.mapClusterer;

    jsFrontend.billboards.currentObjects = [];
    jsFrontend.billboards.mapClusterer = new ymaps.Clusterer({
      //minClusterSize: 10,
      maxZoom: 15
    });

    // Добавляем метки объектов на карту
    objects.forEach(function (billboard) {
      let obj = new ymaps.Placemark(
        [billboard.latitude, billboard.longitude], {
          hasBalloon: false
        });
      obj.info = billboard;

      /** Обработка щелчка мыши на объекте */
      obj.events.add(['click'],  function (e) {
        let objInfo = e.originalEvent.target.info;
        jsFrontend.billboards.showObjectInfo(objInfo);
      });

      jsFrontend.billboards.currentObjects.push(obj);
      jsFrontend.billboards.yandexMap.geoObjects.add(obj);
      jsFrontend.billboards.mapClusterer.add(obj);
    });
    jsFrontend.billboards.yandexMap.geoObjects.add(jsFrontend.billboards.mapClusterer);

    // Ждем несколько секунд (на переключение карты на другой город) и убираем с карты объекты из старого списка
    setTimeout(function () {
      if (oldClusterer)
        jsFrontend.billboards.yandexMap.geoObjects.remove(oldClusterer);

      oldObjects.forEach(function (obj) {
        jsFrontend.billboards.yandexMap.geoObjects.remove(obj);
      })
    }, 3000);

    jsFrontend.billboards.updateBillboardSelector(jsFrontend.billboards.currentObjects);
  },

  /** Выполняет установку центра карты в центр указанного города */
  centerMap: function(cityName = 'Москва') {
    let cityGeocoder = ymaps.geocode(cityName);
    cityGeocoder.then(
      function (res) {
        let city = res.geoObjects.get(0).geometry.getCoordinates();
        jsFrontend.billboards.yandexMap.setCenter(city);
      },
      function (res) {

      }
    );
  },

  /** Закрываем окно с информацией об объекте */
  closeObjectInfo: function() {
    $('#objectInfo').dialog('close');
  },

  /** Выводим имнформацию о объекте */
  showObjectInfo: function (obj) {
    jsFrontend.billboards.yandexMap.setCenter([obj.latitude, obj.longitude]);
    let $dlg = $("#objectInfo" ).dialog({
      resizable: false,
      width: '280px',
      height: 'auto',
      closeText: '',
      draggable: false,

      position: {
        my: "center",
        at: "center",
        of: window
      },
      show: {
        effect: "blind",
        duration: 500
      },
      buttons: [
        {
          text: 'Добавить в заказ!',
          class: 'btn-lg btn-success add-to-cart',
          click: function () {
            jsFrontend.billboards.addToCart(obj)
          }
        }
      ]
    });
    $dlg.siblings('.ui-dialog-titlebar').removeClass('ui-widget-header');

    let $objectInfoPanel = $dlg.find('.object-info-content');
    $objectInfoPanel.html('');
    $objectInfoPanel.append('<b>Адрес: </>' + obj.address + '</b><br>');
    $objectInfoPanel.append('<b>Формат: </b>' + obj.format + '<br>');
    $objectInfoPanel.append('<b>Строна: </b>' + obj.side + '<br>');
    $objectInfoPanel.append('<b>Тип: </b>' + obj.type + '<br>');
    $objectInfoPanel.append('<b>Освещение: </b>' + obj.light + '<br>');
    $objectInfoPanel.append('<b>Материал: </b>' + obj.material + '<br>');
    $objectInfoPanel.append('<b>Стоимость печати: </b>' + obj.printPrice + '<br>');
    $objectInfoPanel.append('<b>Стоимость монтажа: </b>' + obj.workPrice + '<br>');
    $objectInfoPanel.append('<b>Цена от: </b>' + obj.priceFrom + '<br>');

    jsFrontend.billboards.updateAddToCartButton(obj);

    if (!$dlg.dialog('isOpen')) {
      $dlg.dialog('open');
    }
  },

  /** Выполняет обновление селектора объектов */
  updateBillboardSelector: function (objects) {
    let $billboardSelector = $('#billboardSelector');
    $billboardSelector.find('option').remove();

    objects.forEach(function (item) {
      $billboardSelector.append('<option value="' + item.info.id + '">' + item.info.address + '</option>');
    });
    $billboardSelector.selectpicker('refresh');
  },

  /** Настройка селектора выбора объекта */
  initBillboardSelector: function() {
    let $billboardSelector = $('#billboardSelector');
    $billboardSelector.on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
      let billboardId = parseInt(e.currentTarget[clickedIndex].value);
      let obj = jsFrontend.billboards.getObjectById(billboardId);
      if (obj != null) {
        jsFrontend.billboards.showObjectInfo(obj.info);
      }
    });
  },

  /** Получает объект карты по идентификатору */
  getObjectById: function(objectId) {
    for (let objectIndex = 0; objectIndex < jsFrontend.billboards.currentObjects.length; objectIndex++) {
      if (jsFrontend.billboards.currentObjects[objectIndex].info.id == objectId)
        return jsFrontend.billboards.currentObjects[objectIndex];
    }
    return null;
  },

  /** Очистка корзины */
  clearCart: function () {
    jsFrontend.billboards.cartContent = [];
    $('.cart-content').html('').append('<hr>');
    jsFrontend.billboards.updateAddToCartButton();
  },

  /** Иницализация корзины */
  initCart: function() {
    jsFrontend.billboards.$cart = $('#cart').dialog({
      resizable: false,
      height: window.innerHeight,
      width: '300px',
      closeText: '',
      draggable: false,
      autoOpen: false,
      position: {
        my: "right top",
        at: "right top",
        of: window
      },
      show: {
        effect: "drop",
        duration: 500,
        direction: 'right'
      },
      buttons: [
        {
          text: 'Заказать!',
          class: 'btn-lg btn-danger order-btn',
          disabled: true,
          click: function () {
            jsFrontend.billboards.openOrderDialog();
          }
        },
        {
          text: 'Очистить корзину',
          class: 'btn-lg btn-secondary clear-cart-btn',
          disabled: true,
          click: function () {
            jsFrontend.billboards.clearCart();
          }
        }
      ]
    });
    jsFrontend.billboards.$cart.siblings('.ui-dialog-titlebar').removeClass('ui-widget-header');
    jsFrontend.billboards.$cart.dialog('close');

    $('.cart-button').click(function () {
      jsFrontend.billboards.showCart();
    });
  },

  /** Добавление товара в корзину */
  addToCart: function(item) {
    let $cartContent = $('#cartContent');
    $cartContent.append([
      '<div class="item-content">',
      ' <button id="remove-' + item.id +'" type="button" class="btn-sm btn-danger float-right">X</button><br>',
      ' <h6>' + item.address + '</h6><br>',
      ' <b>Формат, м.:</b>' + item.format + '<br>',
      ' <b>Сторона: </b>' + item.side + '<br>',
      ' <b>Тип: </b>' + item.type + '<br>',
      ' <b>Освещение: </b>' + item.light + '<br>',
      ' <b>Материал: </b>' + item.material + '<br>',
      ' <b>Стоимость печати: </b>' + item.printPrice + '<br>',
      ' <b>Стоимость монтажа: </b>' + item.workPrice + '<br>',
      ' <b>Цена от: </b>' + item.priceFrom + '</br>',
      '<hr>',
      '</div>',
    ].join(''));

    /** Обработчик удаления рекламы из корзины */
    $cartContent.find('#remove-' + item.id).on('click', function () {
      $(this).parent().remove();
      jsFrontend.billboards.cartContent = jsFrontend.billboards.cartContent.filter(function (cartItem) {
        return cartItem.id != item.id;
      })
      jsFrontend.billboards.updateAddToCartButton();
    });

    jsFrontend.billboards.cartContent.push(item);
    jsFrontend.billboards.updateAddToCartButton(item);
  },

  /** Выполняем обновление статуса кнопки добавления в корзину */
  updateAddToCartButton: function(item) {
    let $addToCartButton = $('.add-to-cart');
    if ($.inArray(item, jsFrontend.billboards.cartContent) == -1) {
      $addToCartButton.removeClass('btn-secondary')
      $addToCartButton.addClass('btn-success');
      $addToCartButton.attr("disabled", false).removeClass("ui-state-disabled")
      $addToCartButton.html('Добавить в заказ!');
    } else {
      $addToCartButton.addClass('btn-secondary')
      $addToCartButton.removeClass('btn-success');
      $addToCartButton.attr("disabled", true).addClass("ui-state-disabled")
      $addToCartButton.html('Уже в заказе...');
    }

    if (jsFrontend.billboards.cartContent.length > 0) {
      $('.order-btn').attr("disabled", false).removeClass("ui-state-disabled");
      $('.clear-cart-btn').attr("disabled", false).removeClass("ui-state-disabled");
    } else {
      $('.order-btn').attr("disabled", true).addClass("ui-state-disabled");
      $('.clear-cart-btn').attr("disabled", true).addClass("ui-state-disabled");
    }
  },

  /** Показываем окно с содержимым корзины. */
  showCart: function () {
    jsFrontend.billboards.$cart = $('#cart').dialog('open');
  },

  /** Выполняет открытие диалога оформления заказа */
  openOrderDialog: function () {
    let $dlg = $('#orderDialog').dialog({
      width: '550px',
      height: 'auto',
      closeText: '',
      resizable: false,
      modal: true,
      buttons: [
        {
          text: 'Заказать!',
          class: 'btn-lg btn-danger',
          click: function () {
            if (jsFrontend.billboards.checkOrderForm()) {
              $dlg.dialog('close');
              jsFrontend.billboards.processOrder();
            }
          }
        }
      ]
    });
    $dlg.siblings('.ui-dialog-titlebar').removeClass('ui-widget-header');
  },

  /** Проверка заполненности полей формы заказа */
  checkOrderForm: function() {

    return true;
  },

  /** Процедура принятия заказа */
  processOrder: function() {
    $dlg = $('#orderCompleted').dialog({
      width: '200px',
      height: 'auto',
      closeText: '',
      resizable: false,
      modal: true,
      buttons: [
        {
          text: 'Ok',
          class: 'btn-lg btn-success',
          click: function () {
            $dlg.dialog('close');
          }
        }
      ]
    });
    $dlg.siblings('.ui-dialog-titlebar').removeClass('ui-widget-header');
  }
};

$(document).ready(function() {
  jsFrontend.billboards.init();
});

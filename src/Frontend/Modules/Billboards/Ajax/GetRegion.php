<?php

namespace Frontend\Modules\Billboards\Ajax;

use Backend\Core\Engine\Base\AjaxAction;
use Backend\Modules\Billboards\Domain\Billboard\Billboard;
use Backend\Modules\Billboards\Domain\Billboard\BillboardRepository;
use Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegion;
use Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegionRepository;
use Symfony\Component\HttpFoundation\Response;


class GetRegion extends AjaxAction
{
    private const INCORRECT_REGION_ID = 'Некорректное значение кода региона.';

    /** @var BillboardRegionRepository Репозиторий для управления регионами. */
    private $regionRepository;

    /** @var BillboardRepository Репозиторий рекламных щитов. */
    private $billboardRepository;

    public function execute(): void
    {
        set_time_limit(600);

        parent::execute();

        $regionId = $this->getRequest()->get('region_id', 0);
        if ($regionId === 0) {
            $this->output(Response::HTTP_BAD_REQUEST, [self::INCORRECT_REGION_ID]);
        }

        $this->regionRepository = $this->get('doctrine')->getRepository(BillboardRegion::class);
        $this->billboardRepository = $this->get('doctrine')->getRepository(Billboard::class);

        $region = $this->regionRepository->findOneById($regionId);

        $result = [
            'region_name' => $region->getName(),
            'billboards' => $this->billboardRepository->getBillboardsByRegionId($regionId),
            'formats' => $this->billboardRepository->getFormatsByRegionId($regionId)
        ];

        $this->output(Response::HTTP_OK, $result);
    }
}

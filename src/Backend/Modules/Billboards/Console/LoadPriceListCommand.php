<?php

namespace Backend\Modules\Billboards\Console;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Modules\Billboards\Domain\BillboardPriceList\BillboardPriceList;
use Backend\Modules\Billboards\Domain\BillboardPriceList\BillboardPriceListRepository;

class LoadPriceListCommand extends Command
{
    /** @var BillboardPriceListRepository Репозиторий для работы с прайс-листами. */
    private $priceListRepository;

    protected function configure()
    {
        parent::configure();

        $this->setName('billboards:price_list:load')
            ->setDescription('Load billboards price list for selected city')
            ->setDefinition(
                new InputDefinition([
                    new InputOption('price-list', 'p', InputOption::VALUE_REQUIRED)
                ])
            );

    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->priceListRepository = BackendModel::get('doctrine')->getRepository(BillboardPriceList::class);
        $priceListId = $input->getOptions()['price-list'] ?? null;

        if ($priceListId != 'all') {
            $priceListId = intval($priceListId);

            if (!$priceListId || $priceListId <= 0) {
                $this->printUsage();
                return;
            }

            $priceList = $this->priceListRepository->findOneById($priceListId);
            if (!$priceList) {
                $output->writeln('Unknown price list ID');
                return;
            }

            $output->writeln('Processing price list: ' . $priceListId);

            $output->write('Downloading price list file...');
            $this->priceListRepository->loadPriceListFile($priceList);
            $output->writeln('Completed');

            $output->write('Processing price list file...');
            $this->priceListRepository->parseXlsPriceList($priceListId);
            $output->writeln('Completed');

            $output->writeln('Completed!');
        } else {
            $this->priceListRepository->startPriceListImportCommand($output);
        }
    }

    private function printUsage(): void
    {

    }
}

<?php

namespace Backend\Modules\Billboards\Domain\BillboardPriceList;

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Doctrine\ORM\EntityRepository;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Modules\Billboards\Domain\Billboard\Billboard;
use Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegion;
use Backend\Modules\Billboards\Domain\BillboardPriceListCategory\BillboardPriceListCategory;

/**
 * Репозиторий для управления прайс-листами.
 */
class BillboardPriceListRepository extends EntityRepository
{
    /**
     * Выполняет добавление нового прайс-листа.
     *
     * @param BillboardPriceList $priceList Добавляемый прайс-лист.
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(BillboardPriceList $priceList): void
    {
        $this->getEntityManager()->persist($priceList);
        $this->getEntityManager()->flush();
    }

    /**
     * Выполняет сохранение прайс-листа.
     *
     * @param BillboardPriceList $priceList Сохраняемый прайс-лист.
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(BillboardPriceList $priceList): void
    {
        $this->getEntityManager()->persist($priceList);
        $this->getEntityManager()->flush();
    }

    /**
     * Возвращает массив всех имеющихся в наличии прайс-листов.
     *
     * @return array Найденные прайс-листы.
     */
    public function getAllPriceLists(): array
    {
        $priceLists = [];
        foreach ($this->findAll() as $priceList) {
            $lastImported = $priceList->getLastImported();
            if ($lastImported) {
                $lastImported->setTimezone(new \DateTimeZone('MSK'));
                $lastImported = $lastImported->format('d.m.Y H:i:s');
            }

            $lastParsed = $priceList->getLastParsed();
            if ($lastParsed) {
                $lastParsed->setTimezone(new \DateTimeZone('MSK'));
                $lastParsed = $lastParsed->format('d.m.Y H:i:s');
            }

            $priceLists[] = [
                'id' => $priceList->getId(),
                'regionId' => $priceList->getRegionId(),
                'name' => $priceList->getName(),
                'url' => $priceList->getUrl(),
                'filename' => $priceList->getFilename(),
                'lastImported' => $lastImported,
                'lastParsed' => $lastParsed
            ];
        }
        return $priceLists;
    }

    /**
     * Выполняет загрузку файла для прайс-листа.
     *
     * @param BillboardPriceList $priceList
     * @return string Имя загруженного файла.
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function loadPriceListFile(BillboardPriceList $priceList): string
    {
        $options = stream_context_create(['http'=> [
            'timeout' => 600,
            'header'=>'Connection: close\r\n'
        ]]);
        $data = file_get_contents($priceList->getUrl(), false, $options);

        $filename = $this->getStoragePath($priceList) . '/price_list.' . $this->getFileExtension($data);
        file_put_contents($filename, $data);

        $priceList->setFilename(basename($filename));
        $priceList->setLastImported(new \DateTime());
        $this->update($priceList);

        return $filename;
    }

    /**
     * Определяет тип переданных данных.
     * @param $data Анализируемые данные.
     * @return string Расширение, которое необходимо дать файлу с данными.
     */
    private function getFileExtension(&$data): string
    {
        // FIXME: Реализовать проверку типа содержимого.
        return 'xlsx';
    }

    /**
     * Выполняет формирование имени папки для хранения прайс-листа. Если папка не существует, создает ее.
     *
     * @param BillboardPriceList $priceList
     * @return string
     */
    public static function getStoragePath(BillboardPriceList $priceList): string
    {
        $path = FRONTEND_FILES_PATH . '/Billboards/' . $priceList->getRegionId() . '/' . $priceList->getCategoryId();
        $filesystem = new Filesystem();
        $filesystem->mkdir($path);

        return $path;
    }

    /**
     * Выполняет удаление всех прайс-листов.
     */
    public function deleteAll(): void
    {
        $this->getEntityManager()->createQueryBuilder()
            ->delete()
            ->from(BillboardPriceList::class, 'a')
            ->getQuery()
            ->execute();

        BackendModel::get('doctrine')->getRepository(Billboard::class)->deleteAll();
    }

    /**
     * Выполняет формирование справочника прайс-листов.
     */
    public function generatePriceLists(): void
    {
        $this->deleteAll();

        $pricesListCategoryRepository = BackendModel::get('doctrine')->getRepository(BillboardPriceListCategory::class);
        $regionsRepository = BackendModel::get('doctrine')->getRepository(BillboardRegion::class);

        $categories = $pricesListCategoryRepository->getAllPriceListCategories();
        $regions = $regionsRepository->getAllRegions();
        foreach ($regions as $region) {
            foreach ($categories as $category) {
                if (($region['regionId'] !== 1 && !in_array($category['categoryId'], ['0', '1', '2', '3']))
                    || ($region['regionId'] === 1 && in_array($category['categoryId'], ['0', '1', '2', '3']))) {
                    $priceList = new BillboardPriceList([
                        'regionId' => $region['id'],
                        'categoryId' => $category['id'],
                        'name' => $category['name'],
                        'url' => 'https://www.all-billboards.ru/pricelist4.php?region=' . $region['regionId']
                            . ($category['categoryId'] ? '&type=' . $category['categoryId'] : '')
                    ]);
                    $this->add($priceList);

                    //$this->loadPriceListFile($priceList);
                }
            }
        }
    }

    /** @var int Частота фиксации импортируемых данных о щитах.  */
    private const BILLBOARD_BATCH_SIZE = 500;

    /**
     * Производит разбор переданного прайс-листа и сохранение его содержимого в базе данных.
     *
     * @param int $priceListId Загружаемый прайс-лист.
     * @return int Количество импортированных записей.
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function parseXlsPriceList(int $priceListId): int
    {
        $billboardRepository = BackendModel::get('doctrine')->getRepository(Billboard::class);

        $priceList = $this->findOneById($priceListId);
        $filename = FRONTEND_FILES_PATH . '/Billboards/'
            . $priceList->getRegionId() . '/' . $priceList->getCategoryId() . '/' . $priceList->getFilename();

        $reader = new Xlsx();
        $spreadsheet = $reader->load($filename);
        $sheets = $spreadsheet->getAllSheets();

        $importedBillboards = [];

        // Проходим по всем листам документа.
        foreach ($sheets as $sheet) {
            $cells = $sheet->getCellCollection();
            $lastFormat = $sheet->getTitle();

            $columnsMapping = $this->createColumnsMapping($sheet);

            // Проходим по строкам листа
            $lastRow = $cells->getHighestRow();
            for ($row = 22; $row <= $lastRow; $row++) {
                $billboard = new Billboard(['priceListId' => $priceListId]);

                // Проходим по нужным колонкам строки
                $billboardId = null;
                foreach ($columnsMapping as $value => $col) {
                    switch ($value) {
                        case 'Ссылка на фото, карту / код поверхности':
                            $billboardId = $sheet->getCell($col . $row)->getValue();
                            if ($billboardId)
                                $billboard->setBillboardId($billboardId);
                            break;
                        case 'Формат, м.':
                            $format = $sheet->getCell($col . $row)->getValue();
                            if (!$format)
                                $format = $lastFormat;
                            else
                                $lastFormat = $format;

                            $billboard->setFormat($format);
                            break;
                        case 'Адрес':
                            $billboard->setAddress($sheet->getCell($col . $row)->getValue());
                            break;
                        case 'Сторона':
                            $billboard->setSide($sheet->getCell($col . $row)->getValue());
                            break;
                        case 'Тип':
                            $billboard->setType($sheet->getCell($col . $row)->getValue());
                            break;
                        case 'Освещение':
                            $billboard->setLight($sheet->getCell($col . $row)->getValue());
                            break;
                        case 'Материал':
                            $billboard->setMaterial($sheet->getCell($col . $row)->getValue());
                            break;
                        case 'Стоимость печати':
                            $billboard->setPrintPrice($sheet->getCell($col . $row)->getValue());
                            break;
                        case 'Стоимость монтажа':
                            $billboard->setWorkPrice($sheet->getCell($col . $row)->getValue());
                            break;
                        case 'Цена за месяц, руб.':
                            $billboard->setPriceFrom($sheet->getCell($col . $row)->getValue());
                            break;
                    }
                }
                if ($billboardId) {
                    $importedBillboards[] = $billboard;
                }
            }
        }

        // Получаем координаты объектов c сайта-донора
        foreach ($importedBillboards as $billboard) {
            $objectInfo = $this->getObjectInfo($billboard->getBillboardId());
            $billboard->setLatitude($objectInfo['latitude']);
            $billboard->setLongitude($objectInfo['longitude']);
        }

        // Сохраняем объекты в базу данных
        $savedRows = 0;
        $billboardRepository->clearPriceList($priceListId);
        foreach ($importedBillboards as $billboard) {
            $billboardRepository->update($billboard);
            if (++$savedRows % self::BILLBOARD_BATCH_SIZE == 0)
                $billboardRepository->commit();
        }
        $billboardRepository->commit();

        $priceList->setLastParsed(new \DateTime());
        $this->update($priceList);

        return count($importedBillboards);
    }

    /**
     * "Воруем" ключ API с сайта-донора.
     * @param string $billboardId
     * @return string
     */
    private function getApiKey(string $billboardId): string
    {
        $agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
        $url = 'https://www.all-billboards.ru/billboard/' . $billboardId;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        curl_close($ch);

        $keyPosStart = strpos($data, "key:") + 5;
        return substr($data, $keyPosStart, 32);
    }

    /**
     * Выполняет создание маппинга нужных колонок.
     * @param $sheet
     * @return array
     */
    private function createColumnsMapping($sheet): array
    {
        $cells = $sheet->getCellCollection();
        $mapping = [];
        $lastCol = $cells->getHighestColumn();
        for ($col = 'A'; $col <= $lastCol; $col++) {
            $header = $sheet->getCell($col . '20')->getValue();
            if (in_array($header, [
                'Ссылка на фото, карту / код поверхности', 'Формат, м.', 'Адрес', 'Сторона', 'Тип', 'Освещение',
                'Материал', 'Стоимость печати', 'Стоимость монтажа', 'Цена за месяц, руб.'
            ])) {
                $mapping[$header] = $col;
            };
        }
        return $mapping;
    }

    /**
     * Получает об объекте c сайта-донора.
     * @param string $billboardId Код щита.
     * @return array Полученные данные объекта.
     */
    private function getObjectInfo(string $billboardId): array
    {
        $data = BillboardCache::getData($billboardId);

        $latitudePosStart = strpos($data, "'bb_x':");
        $longitudePosStart = strpos($data, "'bb_y':", $latitudePosStart);

        $latitude = $latitudePosStart ? floatval(substr($data, $latitudePosStart + 9, 17)) : 0.0;
        $longitude = $longitudePosStart ? floatval(substr($data, $longitudePosStart + 9, 17)) : 0.0;

        return ([
            'latitude' => $latitude,
            'longitude' => $longitude
        ]);
    }

//    private function getObjectInfo(string $billboardId): array
//    {
//        $agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
//        $apiKey = $this->getApiKey($billboardId);
//        $url = 'https://www.all-billboards.ru/ajax/get_xy.php?id=' . $billboardId . '&key=' . $apiKey;
//
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_VERBOSE, 0);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
//        curl_setopt($ch, CURLOPT_URL, $url);
//        $data = curl_exec($ch);
//        curl_close($ch);
//
//        $latitudePosStart = strpos($data, "bb_x");
//        $longitudePosStart = strpos($data, "bb_y");
//
//        $latitude = $latitudePosStart ? floatval(substr($data, $latitudePosStart + 8, 17)) : 0.0;
//        $longitude = $longitudePosStart ? floatval(substr($data, $longitudePosStart + 8, 17)) : 0.0;
//
//        return ([
//            'latitude' => $latitude,
//            'longitude' => $longitude
//        ]);
//    }

    /**
     * Выполняет запуск потоков импорта прайс-листов.
     * @param OutputInterface $output Поток вывода.
     */
    public function startPriceListImportCommand(OutputInterface $output = null): void
    {
        // Создаем для каждого прайс-листа свой поток импорта.
        $importThreads = [];
        foreach ($this->findAll() as $priceList) {
            $importThread = new Process(
                sprintf('bin/console billboards:price_list:load --price-list=%d', $priceList->getId()),
                BACKEND_PATH . '/../../');
            $importThread->setTimeout(36000);
            $importThread->setIdleTimeout(36000);
            $importThreads[] = $importThread;

            $importThread->start();

            usleep( 5 * 1000000);
        }

        foreach ($importThreads as $importThread) {
            $importThread->wait();
        }
    }
}

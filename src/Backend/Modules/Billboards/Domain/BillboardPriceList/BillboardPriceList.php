<?php

namespace Backend\Modules\Billboards\Domain\BillboardPriceList;

use Doctrine\ORM\Mapping as ORM;

/**
 * Сведения об имеющихся прайс-листах.
 *
 * @ORM\Entity(repositoryClass="Backend\Modules\Billboards\Domain\BillboardPriceList\BillboardPriceListRepository")
 * @ORM\Table(name="billboard_price_lists")
 */
class BillboardPriceList
{
    /**
     * Конструктор класса.
     *
     * @param array $priceListData Информация для инциализации объекта.
     */
    public function __construct(array $priceListData)
    {
        $this->regionId = $priceListData['regionId'] ?? 0;
        $this->categoryId = $priceListData['categoryId'] ?? '';
        $this->name = $priceListData['name'] ?? '';
        $this->url = $priceListData['url'] ?? '';
    }

    /**
     * @var int Идентификатор записи
     *
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var int Идентификатор региона.
     *
     * @ORM\Column(type="integer", name="region_id")
     */
    private $regionId;

    /**
     * @var int Категория прайс-листа.
     *
     * @ORM\Column(type="integer", name="category_id")
     */
    private $categoryId;

    /**
     * @var string Название прайс-листа.
     *
     * @ORM\Column(type="string", length=250)
     */
    private $name;

    /**
     * @var string URL для загрузки прайс-листа.
     *
     * @ORM\Column(type="string", length=500)
     */
    private $url;

    /**
     * @var string Локальное имя файла.
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $filename;

    /**
     * @var \DateTime Дата/Время последнего импорта файла.
     * @ORM\Column(type="datetime", name="last_imported", nullable=true)
     */
    private $lastImported;

    /**
     * @var \DateTime Дата/Время последней обработки прайса.
     * @ORM\Column(type="datetime", name="last_parsed", nullable=true)
     */
    private $lastParsed;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getRegionId(): int
    {
        return $this->regionId;
    }

    /**
     * @param int $region_id
     */
    public function setRegionId(int $region_id): void
    {
        $this->regionId = $region_id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getFilename(): ?string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename(?string $filename): void
    {
        $this->filename = $filename;
    }

    /**
     * @return \DateTime
     */
    public function getLastImported(): ?\DateTime
    {
        return $this->lastImported;
    }

    /**
     * @param \DateTime $lastImported
     */
    public function setLastImported(?\DateTime $lastImported): void
    {
        $this->lastImported = $lastImported;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId(int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return \DateTime
     */
    public function getLastParsed(): ?\DateTime
    {
        return $this->lastParsed;
    }

    /**
     * @param \DateTime $lastParsed
     */
    public function setLastParsed(?\DateTime $lastParsed): void
    {
        $this->lastParsed = $lastParsed;
    }
}

<?php

namespace Backend\Modules\Billboards\Domain\BillboardPriceList;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Кэширование загрузки данных с сайта-донора.
 */
class BillboardCache
{
    public static function getData(int $billboardId): ?string
    {
        $cachedData = self::getCacheData($billboardId);
        return $cachedData ? $cachedData : self::getNetworkData($billboardId);
    }

    private static function getCacheData(int $billboardId): ?string
    {
        $cacheFilename = self::getCacheFolder() . '/' . $billboardId . '.html';
        return file_exists($cacheFilename) ? file_get_contents($cacheFilename) : null;
    }

    private static function getNetworkData(int $billboardId): ?string
    {
        $agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
        $url = 'https://www.all-billboards.ru/billboard/' . $billboardId;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        curl_close($ch);

        $cacheFilename = self::getCacheFolder() . '/' . $billboardId . '.html';
        file_put_contents($cacheFilename, $data);

        return $data;
    }

    private static function getCacheFolder(): string
    {
        $cacheFolder = FRONTEND_FILES_PATH . '/Billboards/billboard_cache';
        $filesystem = new Filesystem();
        $filesystem->mkdir($cacheFolder);

        return $cacheFolder;
    }
}

<?php

namespace Backend\Modules\Billboards\Domain\BillboardRegion;

use Backend\Modules\Billboards\Domain\Billboard\Billboard;
use Backend\Modules\Billboards\Domain\BillboardPriceList\BillboardPriceList;
use Doctrine\ORM\EntityRepository;

final class BillboardRegionRepository extends EntityRepository
{
    /**
     * Выполняет добавление нового региона.
     *
     * @param BillboardRegion $region Сведения о добавляемом регионе.
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(BillboardRegion $region)
    {
        $this->getEntityManager()->persist($region);
        $this->getEntityManager()->flush();
    }

    /**
     * Получает список всех имеющихся регионов.
     *
     * @return array Массив выбранных регионов.
     */
    public function getAllRegions(): array
    {
        $queryResult = $this->findAll();

        usort($queryResult, function (BillboardRegion $region1, BillboardRegion $region2) {
            if ($region1->getName() === $region2->getName())
                return 0;
            return ($region1->getName() > $region2->getName()) ? 1 : -1;
        });

        $regions = [];
        foreach ($queryResult as $region) {
            $regions[] = [
                'id' => $region->getId(),
                'regionName' => $region->getName(),
                'regionId' => $region->getRegionId()
            ];
        }

        return $regions;
    }

    /**
     * Сохранение записи в базе данных.
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(): void
    {
        $this->getEntityManager()->flush();
    }

    /**
     * Выполняет удаление региона из базы данных.
     *
     * @param BillboardRegion $region Удаляемый регион.
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(BillboardRegion $region): void
    {
        $em = $this->getEntityManager();
        $em->remove($region);
        $em->flush();
    }

    /**
     * Удаляет все записи из справочника регионов.
     */
    public function deleteAll(): void
    {
        $this->clear();
        $this->getEntityManager()->flush();
    }

    /**
     * Формирует массив регионов.
     *
     * @return array Массив регионов. В качестве индексов используются коды регионов.
     */
    public function getRegionsMapping(): array
    {
        $regions = [];
        foreach ($this->findAll() as $region) {
            $regions[$region->getId()] = $region->getName();
        }
        return $regions;
    }
}

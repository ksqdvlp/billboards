<?php

namespace Backend\Modules\Billboards\Domain\BillboardRegion;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Backend\Core\Language\Language;

/**
 * Описание формы добавления региона
 */
class BillboardRegionType extends AbstractType
{
    /**
     * Построение формы добавления региона
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add(
            'name',
            TextType::class,
            [
                'label' => Language::lbl('RegionName')
            ]
        )->add(
            'regionId',
            IntegerType::class,
            [
                'label' => Language::lbl('RegionId')
            ]
        );
    }
}

<?php

namespace Backend\Modules\Billboards\Domain\BillboardRegion;

use Doctrine\ORM\Mapping as ORM;

/**
 * Регионы
 *
 * @ORM\Entity(repositoryClass="Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegionRepository")
 * @ORM\Table(name="billboard_regions")
 */
class BillboardRegion
{
    /**
     * @var int Внутренний идентификатор региона
     *
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string Название региона
     *
     * @ORM\Column(type="string", length=250)
     */
    private $name;

    /**
     * @var int Код региона
     *
     * @ORM\Column(type="integer", name="region_id")
     */
    private $regionId;

    /**
     * Конструктор класса. Создает новый объект на основе переданного массива данных.
     *
     * @param array $formData Данные для инициализации нового объекта.
     */
    public function __construct(array $formData)
    {
        $this->name = $formData['name'] ?? '';
        $this->regionId = isset($formData['regionId']) ? intval($formData['regionId']) : 0;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getRegionId(): int
    {
        return $this->regionId;
    }

    /**
     * @param int $regionId
     */
    public function setRegionId(int $regionId): void
    {
        $this->regionId = $regionId;
    }
}

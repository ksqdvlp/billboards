<?php

namespace Backend\Modules\Billboards\Domain\BillboardPriceListCategory;

use Doctrine\ORM\Mapping as ORM;

/**
 * Категории прайс-листов.
 *
 * @ORM\Entity(repositoryClass="Backend\Modules\Billboards\Domain\BillboardPriceListCategory\BillboardPriceListCategoryRepository")
 * @ORM\Table(name="billboard_price_list_category")
 */
class BillboardPriceListCategory
{
    /**
     * Конструктор класса.
     * @param array $priceListCategoryData Данные для инциализации экземпляра класса.
     */
    public function __construct(array $priceListCategoryData)
    {
        $this->categoryId = $priceListCategoryData['categoryId'];
        $this->name = $priceListCategoryData['name'] ?? '';
    }

    /**
     * @var int Идентификатор записи
     *
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string Бизнес-идентификатор категории.
     *
     * @ORM\Column(type="string", name="category_id", length=20, nullable=true)
     */
    private $categoryId;

    /**
     * @var string Название категории.
     *
     * @ORM\Column(type="string", length=250)
     */
    private $name;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCategoryId(): ?string
    {
        return $this->categoryId;
    }

    /**
     * @param string $categoryId
     */
    public function setCategoryId(?string $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}

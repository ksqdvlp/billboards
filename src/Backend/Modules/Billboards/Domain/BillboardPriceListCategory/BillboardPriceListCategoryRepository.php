<?php

namespace Backend\Modules\Billboards\Domain\BillboardPriceListCategory;

use Doctrine\ORM\EntityRepository;

/**
 * Репозиторий для управления категориями прайс-листов.
 */
class BillboardPriceListCategoryRepository extends EntityRepository
{
    /**
     * Выполняет сохранение сведений о категории прайс-листа.
     * @param BillboardPriceListCategory $priceListCategory Сохраняемая категория.
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(BillboardPriceListCategory $priceListCategory): void
    {
        $this->getEntityManager()->persist($priceListCategory);
        $this->getEntityManager()->flush();
    }

    /**
     * Формирует польный список категорий прайс-листов.
     * @return array Список имеющихся категорий прайс-листов.
     */
    public function getAllPriceListCategories(): array
    {
        $categories = [];
        foreach ($this->findAll() as $category) {
            $categories[] = [
                'id' => $category->getId(),
                'categoryId' => $category->getCategoryId(),
                'name' => $category->getName()
            ];
        }
        return $categories;
    }
}

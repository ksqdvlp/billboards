<?php

namespace Backend\Modules\Billboards\Domain\BillboardPriceListCategory;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Backend\Core\Language\Language;

class BillboardPriceListCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add(
            'categoryId',
            TextType::class,
            [
                'label' => Language::lbl('PriceListCategoryId'),
                'required' => false
            ]
        )->add(
            'name',
            TextType::class,
            [
                'label' => Language::lbl('PriceListCategoryName')
            ]
        );
    }
}

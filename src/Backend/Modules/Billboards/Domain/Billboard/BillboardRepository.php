<?php

namespace Backend\Modules\Billboards\Domain\Billboard;

use Backend\Modules\Billboards\Domain\BillboardPriceList\BillboardPriceList;
use Doctrine\ORM\EntityRepository;

class BillboardRepository extends EntityRepository
{
    /**
     * Выполняет сохранение данных о рекламном щите.
     * @param Billboard $billboard Сведения о щите.
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(Billboard $billboard): void
    {
        $this->getEntityManager()->persist($billboard);
    }

    /**
     * Выполняет удаление цен для указанного прайс-листа.
     * @param int $priceListId Код прайс-листа, для которого нужно удалить цены.
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function clearPriceList(int $priceListId): void
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->delete(Billboard::class, 'a')
            ->where('a.priceListId = :priceListId')
            ->setParameter(':priceListId', $priceListId)
            ->getQuery()
            ->execute();
        $this->getEntityManager()->flush();
    }

    public function commit(): void
    {
        $this->getEntityManager()->flush();
    }

    /**
     * Возвращает список имеющихся форматов щитов для указанного региона.
     * @param int $regionId Код региона.
     * @return array Справочник форматов для региона.
     */
    public function getFormatsByRegionId(int $regionId): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $query = $queryBuilder->select('b.format')
            ->from(Billboard::class, 'b')
            ->innerJoin(BillboardPriceList::class, 'p',
                \Doctrine\ORM\Query\Expr\Join::WITH, 'p.id = b.priceListId')
            ->where("p.regionId = $regionId")
            ->groupBy('b.format')
            ->orderBy('b.format')
            ->getQuery();

        return array_values($query->execute());
    }

    /**
     * Возвращает список рекламных щитов для указанного региона.
     * @param int $regionId Код региона.
     * @return array Сформированный справочник рекламных щитов.
     * FIXME: Заказчик просил сделать увеличение цены рекламы
     * FIXME: Попробовать оптимизировать
     */
    public function getBillboardsByRegionId(int $regionId): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $query = $queryBuilder->select('b')
            ->from(Billboard::class, 'b')
            ->innerJoin(BillboardPriceList::class, 'p',
                \Doctrine\ORM\Query\Expr\Join::WITH, 'p.id = b.priceListId')
            ->where("p.regionId = $regionId")
            ->getQuery();

        $queryResult = $query->execute();
        $billboards = [];
        foreach ($queryResult as $item) {
            $billboards[] = [
                'id' => $item->getId(),
                'priceListId' => $item->getPriceListId(),
                'format' => $item->getFormat(),
                'latitude' => $item->getLatitude(),
                'longitude' => $item->getLongitude(),
                'address' => $item->getAddress(),
                'side' => $item->getSide(),
                'type' => $item->getType(),
                'light' => $item->getLight(),
                'material' => $item->getMaterial(),
                'printPrice' => $item->getPrintPrice(),
                'workPrice' => $item->getWorkPrice(),
                'priceFrom' => $item->getWorkPrice()
            ];
        }
        return $billboards;
    }

    /**
     * Выполняет удаление всех рекламных щитов.
     */
    public function deleteAll(): void
    {
        $this->getEntityManager()->createQueryBuilder()
            ->delete()
            ->from(Billboard::class, 'a')
            ->getQuery()
            ->execute();
    }

}

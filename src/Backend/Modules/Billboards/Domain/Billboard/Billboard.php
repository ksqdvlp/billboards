<?php

namespace Backend\Modules\Billboards\Domain\Billboard;

use Doctrine\ORM\Mapping as ORM;

/**
 * Сведения о рекламном щите.
 * @ORM\Entity(repositoryClass="Backend\Modules\Billboards\Domain\Billboard\BillboardRepository")
 * @ORM\Table(name="billboards", indexes={@ORM\Index(name="price_list_idx", columns={"price_list_id"})})
 */
class Billboard
{
    public function __construct(array $data)
    {
        $this->format = $data['format'] ?? '';
        $this->billboardId = $data['billboardId'] ?? '';
        $this->priceListId = $data['priceListId'] ?? 0;
        $this->latitude = $data['latitude'] ?? 0;
        $this->longitude = $data['longitude'] ?? 0;
        $this->address = $data['address'] ?? '';
        $this->side = $data['side'] ?? '';
        $this->type = $data['type'] ?? '';
        $this->light = $data['light'] ?? '';
        $this->material = $data['material'] ?? '';
        $this->printPrice = intval($data['priceListId'] ?? '');
        $this->workPrice = $data['workPrice'] ?? '';
        $this->priceFrom = intval($data['priceFrom'] ?? '0');
    }

    /**
     * @var int Идентификатор записи.
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var int Идентификатор прайс-листа.
     * @ORM\Column(type="integer", name="price_list_id")
     */
    private $priceListId;

    /**
     * @var float Широта
     * @ORM\Column(type="float")
     */
    private $latitude;

    /**
     * @var float Долгота.
     * @ORM\Column(type="float")
     */
    private $longitude;

    /**
     * @var string Формат.
     * @ORM\Column(type="string", length=100)
     */
    private $format;

    /**
     * @var string Код конструкции.
     * @ORM\Column(type="string", length=20, name="billboard_id")
     */
    private $billboardId;

    /**
     * @var string Адрес объекта
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $address;

    /**
     * @var string Сторона
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $side;

    /**
     * @var string Тип
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $type;

    /**
     * @var string Освещение
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $light;

    /**
     * @var string Материал
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $material;

    /**
     * @var string Стоимость печати
     * @ORM\Column(type="string", length=30, name="print_price", nullable=true)
     */
    private $printPrice;

    /**
     * @var string Стоимость монтажа
     * @ORM\Column(type="string", name="work_price", length=30, nullable=true)
     */
    private $workPrice;

    /**
     * @var string Цена от
     * @ORM\Column(type="string", name="price_from", length=30, nullable=true)
     */
    private $priceFrom;

    /**
    private $imageFile;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat(string $format): void
    {
        $this->format = $format;
    }

    /**
     * @return string
     */
    public function getBillboardId(): string
    {
        return $this->billboardId;
    }

    /**
     * @param string $billboardId
     */
    public function setBillboardId(string $billboardId): void
    {
        $this->billboardId = $billboardId;
    }

    /**
     * @return int
     */
    public function getPriceListId(): int
    {
        return $this->priceListId;
    }

    /**
     * @param int $priceListId
     */
    public function setPriceListId(int $priceListId): void
    {
        $this->priceListId = $priceListId;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     */
    public function setLatitude(float $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude(float $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getSide(): ?string
    {
        return $this->side;
    }

    /**
     * @param string $side
     */
    public function setSide(?string $side): void
    {
        $this->side = $side;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getLight(): ?string
    {
        return $this->light;
    }

    /**
     * @param string $light
     */
    public function setLight(?string $light): void
    {
        $this->light = $light;
    }

    /**
     * @return string
     */
    public function getMaterial(): ?string
    {
        return $this->material;
    }

    /**
     * @param string $material
     */
    public function setMaterial(?string $material): void
    {
        $this->material = $material;
    }

    /**
     * @return string|null
     */
    public function getPrintPrice(): ?string
    {
        return $this->printPrice;
    }

    /**
     * @param string|null $printPrice
     */
    public function setPrintPrice(?string $printPrice): void
    {
        $this->printPrice = $printPrice;
    }

    /**
     * @return string
     */
    public function getWorkPrice(): ?string
    {
        return $this->workPrice;
    }

    /**
     * @param string $workPrice
     */
    public function setWorkPrice(?string $workPrice): void
    {
        $this->workPrice = $workPrice;
    }

    /**
     * @return int
     */
    public function getPriceFrom(): ?string
    {
        return $this->priceFrom;
    }

    /**
     * @param string $priceFrom
     */
    public function setPriceFrom(?string $priceFrom): void
    {
        $this->priceFrom = $priceFrom;
    }
}

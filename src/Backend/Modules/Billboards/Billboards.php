<?php

namespace Backend\Modules\Billboards;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class Billboards extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
    }
}

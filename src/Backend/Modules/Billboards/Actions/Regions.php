<?php

namespace Backend\Modules\Billboards\Actions;

use Backend\Core\Engine\Base\ActionIndex as BaseActionIndex;
use Backend\Core\Engine\DataGridArray as BackendDataGridArray;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Core\Language\Language;
use Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegion;

/**
 * Список регионов
 */
class Regions extends BaseActionIndex
{
    public function execute(): void
    {
        parent::execute();

        $this->loadDataGrid();
        $this->parse();
        $this->display();
    }

    /**
     * Формирование списка регионов
     */
    private function loadDataGrid(): void
    {
        $regions = $this->get('doctrine')->getRepository(BillboardRegion::class)->getAllRegions();
        $this->dataGrid = new BackendDataGridArray($regions);

        $this->dataGrid->addColumnAction(
            'edit',
            null,
            Language::lbl('Edit'),
            BackendModel::createUrlForAction('EditRegion') . '&amp;id=[id]',
            Language::lbl('Edit')
        );

        $this->dataGrid->setPagingLimit(15);
        $this->dataGrid->setSortingColumns(['regionName', 'regionId'], 'regionName');
    }

    /**
     * Передача данных шаблонизатору
     */
    protected function parse(): void
    {
        parent::parse();

        $this->template->assign('regionsGrid', $this->dataGrid->getContent());
    }
}

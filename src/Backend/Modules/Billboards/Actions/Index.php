<?php

namespace Backend\Modules\Billboards\Actions;

use Backend\Core\Engine\Base\ActionIndex as BaseActionIndex;
use Backend\Core\Engine\Model;

/**
 * Стартовая страница админки карты рекламных щитов
 */
class Index extends BaseActionIndex
{
    public function execute(): void
    {
        parent::execute();

        $this->redirect(Model::createUrlForAction('Regions'));
    }
}

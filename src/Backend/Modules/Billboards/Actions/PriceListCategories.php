<?php

namespace Backend\Modules\Billboards\Actions;

use Backend\Core\Engine\Base\ActionIndex as BaseActionIndex;
use Backend\Core\Engine\DataGridArray as BackendDataGridArray;
use Backend\Modules\Billboards\Domain\BillboardPriceListCategory\BillboardPriceListCategory;

/**
 * Список категорий прайс-листов.
 */
class PriceListCategories extends BaseActionIndex
{
    public function execute(): void
    {
        parent::execute();

        $this->loadDataGrid();
        $this->parse();
        $this->display();
    }

    private function loadDataGrid(): void
    {
        $categories = $this->get('doctrine')
            ->getRepository(BillboardPriceListCategory::class)
            ->getAllPriceListCategories();
        $this->dataGrid = new BackendDataGridArray($categories);
    }

    protected function parse(): void
    {
        parent::parse();

        $this->template->assign('priceListCategoriesGrid', $this->dataGrid->getContent());
    }
}

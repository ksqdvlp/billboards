<?php


namespace Backend\Modules\Billboards\Actions;

use Backend\Core\Engine\Base\ActionIndex as BaseActionIndex;
use Backend\Core\Engine\Model;
use Backend\Modules\Billboards\Domain\BillboardPriceList\BillboardPriceList;

/**
 * Формирование списка прайс-листов.
 */
class GeneratePriceLists extends BaseActionIndex
{
    public function execute(): void
    {
        parent::execute();

        $priceListRepository = $this->get('doctrine')->getRepository(BillboardPriceList::class);
        $priceListRepository->generatePriceLists();

        $this->redirect(Model::createUrlForAction('PriceLists'));
    }
}

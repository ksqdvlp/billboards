<?php

namespace Backend\Modules\Billboards\Actions;

use Backend\Core\Engine\Base\ActionDelete as BackendBaseActionDelete;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegion;
use Backend\Modules\Billboards\Form\BillboardRegionDelType;

class DeleteRegion extends BackendBaseActionDelete
{
    public function execute(): void
    {
        parent::execute();

        $deleteForm = $this->createForm(
            BillboardRegionDelType::class,
            null,
            ['module' => $this->getModule()]
        );

        $deleteForm->handleRequest($this->getRequest());
        if ($deleteForm->isSubmitted() && $deleteForm->isValid()) {
            $doctrine = $this->get('doctrine')->getRepository(BillboardRegion::class);

            $id = $deleteForm->getData()['id'];
            $region = $doctrine->findOneById($id);
            $doctrine->delete($region);

            $this->redirect(BackendModel::createUrlForAction('Regions'));
        } else {
            $this->redirect(BackendModel::createUrlForAction(
                'Regions', null, null,
                ['error' => 'something-went-wrong']));
        }
    }
}

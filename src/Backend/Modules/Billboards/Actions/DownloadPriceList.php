<?php

namespace Backend\Modules\Billboards\Actions;

use Backend\Core\Engine\Base\ActionIndex as BaseActionIndex;
use Backend\Core\Engine\Model;
use Backend\Modules\Billboards\Domain\BillboardPriceList\BillboardPriceList;

/**
 * Загрузка прайс-листа
 */
class DownloadPriceList extends BaseActionIndex
{
    public function execute(): void
    {
        set_time_limit(600);

        parent::execute();

        $priceListRepository = $this->get('doctrine')->getRepository(BillboardPriceList::class);
        $id = $this->getRequest()->get('id', 0);
        $priceList = $priceListRepository->findOneById($id);
        $priceListRepository->loadPriceListFile($priceList);
        $priceListRepository->parseXlsPriceList($id);

        $this->redirect(Model::createUrlForAction('PriceLists'));
    }
}

<?php

namespace Backend\Modules\Billboards\Actions;

use Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegionType;
use Backend\Modules\Billboards\Form\BillboardRegionDelType;
use Symfony\Component\Form\Form;
use Backend\Core\Engine\Base\ActionEdit as BackendBaseActionEdit;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegion;
use Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegionRepository;

/**
 * Форма редактирования карточки региона
 */
class EditRegion extends BackendBaseActionEdit
{
    /** @var BillboardRegion Редактируемый регион */
    private $region;

    /** @var BillboardRegionRepository Репозиторий для работы со справочником регионов */
    private $doctrine;

    public function execute(): void
    {
        parent::execute();

        $this->doctrine = $this->get('doctrine')->getRepository(BillboardRegion::class);
        $this->id = $this->getRequest()->get('id');
        $this->region = $this->doctrine->findOneById($this->id);

        $form = $this->getForm();
        if ($form->isSubmitted() && $form->isValid()) {
            $this->doctrine->update();
            $this->redirect(BackendModel::createUrlForAction('Regions'));
        } else {
            $this->parseDeleteForm();
            $this->parseForm($form);
        }
    }

    /**
     * Построение формы Symfony для редактирования карточки региона.
     *
     * @return Form Полученная форма.
     */
    private function getForm(): Form
    {
        $form = $this->createForm(BillboardRegionType::class, $this->region);
        $form->handleRequest($this->getRequest());

        return $form;
    }

    /**
     * Передача формы шаблонизатору.
     *
     * @param Form $form Отображаемая форма.
     */
    private function parseForm(Form $form): void
    {
        $this->template->assign('form', $form->createView());

        $this->parse();
        $this->display();
    }

    /**
     * Построение формы удаления региона.
     */
    private function parseDeleteForm(): void
    {
        $deleteForm = $this->createForm(
            BillboardRegionDelType::class,
            ['id' => $this->id],
            ['module' => $this->getModule()]
        );
        $this->template->assign('deleteForm', $deleteForm->createView());
        $this->template->assign('name', $this->region->getName());
    }
}

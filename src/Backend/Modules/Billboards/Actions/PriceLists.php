<?php

namespace Backend\Modules\Billboards\Actions;

use Backend\Core\Engine\Base\ActionIndex as BaseActionIndex;
use Backend\Core\Engine\DataGridArray as BackendDataGridArray;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Core\Language\Language;
use Backend\Modules\Billboards\Domain\BillboardPriceList\BillboardPriceList;
use Backend\Modules\Billboards\Domain\BillboardPriceList\BillboardPriceListRepository;
use Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegion;
use Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegionRepository;

/**
 * Список прайс-листов.
 */
class PriceLists extends BaseActionIndex
{
    /** @var array Названия регионов. */
    private $regionNames;

    /** @var BillboardRegionRepository Репозиторий для управления справочником регионов. */
    private $regionsRepository;

    /** @var BillboardPriceListRepository Репозиторий для управления справочниками прайс-листов. */
    private $priceListsRepository;

    public function execute(): void
    {
        parent::execute();

        $this->loadReferences();
        $this->loadDataGrid();
        $this->parse();
        $this->display();
    }

    /**
     * Настройка необходимых справочников.
     */
    private function loadReferences(): void
    {
        $this->regionsRepository = $this->get('doctrine')->getRepository(BillboardRegion::class);
        $this->priceListsRepository = $this->get('doctrine')->getRepository(BillboardPriceList::class);

        $this->regionNames = $this->regionsRepository->getRegionsMapping();
    }

    /**
     * Формирование таблицы со списком прайс-листов.
     */
    private function loadDataGrid(): void
    {
        $priceLists = $this->priceListsRepository->getAllPriceLists();
        $this->dataGrid = new BackendDataGridArray($priceLists);

//        // Кнопка редактирование прайс-листа
//        $this->dataGrid->addColumnAction(
//            'edit',
//            null,
//            Language::lbl('Edit'),
//            BackendModel::createUrlForAction('EditPriceList') . '&amp;id=[id]',
//            Language::lbl('Edit')
//        );

        // Кнопка загрузки прайс-листа
        $this->dataGrid->addColumnAction(
            'copy',
            null,
            Language::lbl('Download'),
            BackendModel::createUrlForAction('DownloadPriceList') . '&amp;id=[id]',
            Language::lbl('Download')
        );

        // Для региона заменяем код на название
        $this->dataGrid->setHeaderLabels(['regionId' => Language::lbl('RegionName')]);
        $this->dataGrid->setColumnFunction(
            [$this, 'getRegionName'],
            ['[regionId]'],
            'regionId',
            true
        );

        $this->dataGrid->setColumnURL('url', '[url]');
        $this->dataGrid->setColumnFunction(
            [$this, 'formatFilename'],
            ['[id]'],
            'filename',
            true
        );
    }

    /**
     * Получает название региона по его коду.
     *
     * @param int $regionId Код региона.
     * @return string Название региона.
     */
    public function getRegionName(int $regionId): string
    {
        return $this->regionNames[$regionId] ?? '';
    }

    public function formatFilename(int $id): string
    {
        $priceList = $this->priceListsRepository->findOneById($id);
        return "<a href='" . FRONTEND_FILES_URL . '/Billboards/'
            . $priceList->getRegionId() . '/'
            . $priceList->getCategoryId() . '/'
            . $priceList->getFilename()
            . "'>" . $priceList->getFilename() . '</a>';
    }

    /**
     * Передача данных шаблонизатору.
     */
    protected function parse(): void
    {
        parent::parse();

        $this->template->assign('priceListsGrid', $this->dataGrid->getContent());
    }
}

<?php

namespace Backend\Modules\Billboards\Actions;

use Symfony\Component\Form\Form;
use Backend\Core\Engine\Base\ActionAdd as BackendBaseActionAdd;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Modules\Billboards\Domain\BillboardPriceListCategory\BillboardPriceListCategoryType;
use Backend\Modules\Billboards\Domain\BillboardPriceListCategory\BillboardPriceListCategory;
use Backend\Modules\Billboards\Domain\BillboardPriceListCategory\BillboardPriceListCategoryRepository;

/**
 * Форма добавления категории прайс-листа.
 */
class AddPriceListCategory extends BackendBaseActionAdd
{
    /** @var BillboardPriceListCategoryRepository Репозиторий для управления категориями прайс-листов. */
    private $priceListCategoryRepository;

    public function execute(): void
    {
        parent::execute();

        $this->priceListCategoryRepository = $this->get('doctrine')->getRepository(BillboardPriceListCategory::class);
        $form = $this->getForm();
        if ($form->isSubmitted() && $form->isValid()) {
            $this->saveFormData($form);
            $this->redirect(BackendModel::createUrlForAction('PriceListCategories'));
        } else {
            $this->parseForm($form);
        }
    }

    /**
     * Построение формы Symfony для добавления новой категории прайс-листа.
     * @return Form
     */
    private function getForm(): Form
    {
        $form = $this->createForm(BillboardPriceListCategoryType::class);
        $form->handleRequest($this->getRequest());
        return $form;
    }

    /**
     * Сохранение данных, введенных в форме, в базу данных.
     * @param Form $form
     */
    private function saveFormData(Form $form): void
    {
        $priceListCategoryData = $form->getData();
        $priceListCategory = new BillboardPriceListCategory($priceListCategoryData);

        $this->priceListCategoryRepository->update($priceListCategory);
    }

    /**
     * Передача формы шаблонизатору.
     *
     * @param Form $form
     */
    private function parseForm(Form $form): void
    {
        $this->template->assign('form', $form->createView());

        $this->parse();
        $this->display();
    }
}

<?php

namespace Backend\Modules\Billboards\Actions;

use Symfony\Component\Form\Form;
use Backend\Core\Engine\Base\ActionAdd as BackendBaseActionAdd;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegion;
use Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegionType;

/**
 * Форма добавления нового региона
 */
class AddRegion extends BackendBaseActionAdd
{
    public function execute(): void
    {
        parent::execute();

        $form = $this->getForm();
        if ($form->isSubmitted() && $form->isValid()) {
            $this->saveFormData($form);
            $this->redirect(BackendModel::createUrlForAction('Regions'));
        } else {
            $this->parseForm($form);
        }
    }

    /**
     * Выполняет создание новой формы
     *
     * @return Form Созданная форма
     */
    private function getForm(): Form
    {
        $form = $this->createForm(BillboardRegionType::class);
        $form->handleRequest($this->getRequest());
        return $form;
    }

    /**
     * Передача формы шаблонизатору.
     *
     * @param Form $form
     */
    private function parseForm(Form $form): void
    {
        $this->template->assign('form', $form->createView());

        $this->parse();
        $this->display();
    }

    /**
     * Выполняет сохранение данных переданной формы.
     *
     * @param Form $form Сохраняемая форма.
     */
    private function saveFormData(Form $form): void
    {
        $regionData = $form->getData();
        $region = new BillboardRegion($regionData);

        $this->get('doctrine')->getRepository(BillboardRegion::class)->add($region);
    }
}

<?php

namespace Backend\Modules\Billboards\Installer;

use Common\ModuleExtraType;
use Backend\Core\Engine\Model;
use Backend\Core\Installer\ModuleInstaller;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Modules\Billboards\Domain\BillboardPriceList\BillboardPriceList;
use Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegion;
use Backend\Modules\Billboards\Domain\Billboard\Billboard;
use Backend\Modules\Billboards\Domain\BillboardPriceList\BillboardPriceListRepository;
use Backend\Modules\Billboards\Domain\BillboardPriceListCategory\BillboardPriceListCategory;
use Backend\Modules\Billboards\Domain\BillboardRegion\BillboardRegionRepository;

/**
 * Инсталлятор модуля Billboards
 */
class Installer extends ModuleInstaller
{
    /** @var BillboardRegionRepository Репозиторий регионов */
    private $regions;

    /** @var BillboardPriceListRepository Репозиторий прайс-листов */
    private $priceLists;

    public function install(): void
    {
        $this->addModule('Billboards');
        $this->makeSearchable($this->getModule());
        $this->importLocale(__DIR__ . '/Data/locale.xml', true);

        $this->configureEntities();
        $this->configureBackendRights();
        $this->configureBackendNavigation();
        $this->configureFrontendExtras();
    }

    /**
     * Настройка Doctrine
     */
    private function configureEntities(): void
    {
        Model::get('fork.entity.create_schema')->forEntityClasses(
            [
                Billboard::class,
                BillboardRegion::class,
                BillboardPriceList::class,
                BillboardPriceListCategory::class
            ]
        );

        $this->regions = BackendModel::get('doctrine')->getRepository(BillboardRegion::class);
        $this->priceLists = BackendModel::get('doctrine')->getRepository(BillboardPriceList::class);
    }

    /**
     * Настройка элементов Frontend
     */
    private function configureFrontendExtras(): void
    {
        $this->insertExtra($this->getModule(), ModuleExtraType::widget(), 'BillboardsMap', 'BillboardsMap');
    }

    /**
     * Настройка прав действий в Backend
     */
    private function configureBackendRights(): void
    {
        $this->setModuleRights(1, $this->getModule());
        $this->setActionRights(1, $this->getModule(), 'Index');

        $this->setActionRights(1, $this->getModule(), 'Regions');
        $this->setActionRights(1, $this->getModule(), 'AddRegion');
        $this->setActionRights(1, $this->getModule(), 'EditRegion');
        $this->setActionRights(1, $this->getModule(), 'DeleteRegion');

        $this->setActionRights(1, $this->getModule(), 'PriceListCategories');

        $this->setActionRights(1, $this->getModule(), 'PriceLists');
        $this->setActionRights(1, $this->getModule(), 'AddPriceList');
        $this->setActionRights(1, $this->getModule(), 'EditPriceList');
        $this->setActionRights(1, $this->getModule(), 'DeletePriceList');
        $this->setActionRights(1, $this->getModule(), 'DownloadPriceList');
    }

    /**
     * Настройка навигации в Backend
     */
    private function configureBackendNavigation(): void
    {
        $navigationModuleId = $this->setNavigation(null, 'Modules');
        $navigationBillboardsId = $this->setNavigation($navigationModuleId, $this->getModule());

        // Справочник регионов
        $this->setNavigation(
            $navigationBillboardsId,
            'Regions',
            'billboards/regions',
            [
                'billboards/add_region',
                'billboards/edit_region',
                'billboards/delete_region'
            ]
        );

        // Справочник категорий прайс-листов
        $this->setNavigation(
            $navigationBillboardsId,
            'PriceListCategories',
            'billboards/price_list_categories',
            [
                'billboards/add_price_list_category',
                'billboards/edit_price_list_category',
                'billboards/delete_price_list_category'
            ]
        );

        // Справочник прайс-листов
        $this->setNavigation(
            $navigationBillboardsId,
            'PriceLists',
            'billboards/price_lists',
            [
                'billboards/add_price_list',
                'billboards/edit_price_list',
                'billboards/delete_price_list'
            ]
        );
    }

    /**
     * Первоначальное заполнение справочника регионов.
     */
    private function loadRegionsData(): void
    {
        if ($this->regions->count([]) > 0) return;

        foreach ($this->regionsData as $regionData) {
            $this->regions->add(new BillboardRegion($regionData));
        }
    }
}
